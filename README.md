# Chess - a working build of Chess implemented in c++
A simple build of Chess with the rules, piece movement and a console UI in C++.
## Table of contents
 * [General info](#general-info)
 * [Technlogies](#technologies)
 * [Setup](#setup)


## General info
 I Used this project to learn C++ and OOP to a pretty deep and good understanding.
 It's built upon the idea of a super-class representing a base game piece that the other pieces inherit from.
 From there the game is played using the objects of those pieces and a game board class, and these classes organize the code
 and guild the game and it's rules.


## Technologies
 - C++ 11

## Setup
 To run this projet please go to the patch-v1 branch and from there, you can download the executable file and run it.
